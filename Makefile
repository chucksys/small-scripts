EXE := random-caps space-out rot13

all: $(EXE)

install:
	find . -maxdepth 1 -type f -executable -exec cp -i {} ~/bin/ \;

%: %.c
	cc -O2 -o $@ $<

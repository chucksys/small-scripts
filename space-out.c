#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define BUFFER_SIZE 4096
#define CAPS_PROBABILITY 50

char randomCaps(char c) {
	if (isalpha(c)) {
		return rand() % 100 >= CAPS_PROBABILITY ? toupper(c) : c;
	} else {
		return c;
	}
}

int main() {
	char buffer[BUFFER_SIZE * 2 + 1];
	memset(buffer, 0, BUFFER_SIZE * 2 + 1);
	while (read(STDIN_FILENO, buffer, BUFFER_SIZE)) {
		for (size_t i = BUFFER_SIZE - 1; i > 0; --i) {
			buffer[i * 2] = buffer[i];
			buffer[i * 2 + 1] = ' ';
		}

		buffer[1] = ' ';

		printf("%s", buffer);
	}
}

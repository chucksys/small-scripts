# Small Scripts

A repository full of small scripts that help automate the boring stuff, like:

- RANdOm CApiTaLIZatiOn
- s p a c e   l e t t e r s   o u t
- rot13

The intention for these simple scripts (that don't have any documentation
whatsoever) is for fun and for fun only.

One of the ways I use these scripts is to use them in Unix-style pipes so that
I can chain them together with other commands. For example:

```
# Randomly capitalize anything in clipboard
bindsym control+shift+m exec xclip -out | ~/bin/random-caps | xclip -in
```

# Build

Building probably requires a typical unix system with some C compiler
installed.

```bash
make
```

To install (read: copy into `/home/<user>/bin` for easy use), do the following:

```bash
make install
```

If you just want one of them, be specific about the make:

```bash
make random-caps
```

Note that if you now do a `make install`, it only copies the executables that
are already in the directory. This means that any other stray executables are
also copied as a result. Of course, we only copy interactively, so that if you
have the same-named executable, you can choose to overwrite it (or not).
